import {
	HttpModule,
	HttpService
} from "@nestjs/axios";
import {
	DynamicModule,
	Module
} from "@nestjs/common";
import {
	ClientsModule,
	Transport
} from "@nestjs/microservices";
import * as fs from "fs";
import { firstValueFrom } from "rxjs";
import { DoubleClientModuleOptions } from "../models/double-client-module.options";
import {
	generateInterfaces,
	getPackage
} from "../utils/proto.utils";

@Module({
	imports: [HttpModule]
})
export class DoubleClientModule {
	public static async register(options: DoubleClientModuleOptions): Promise<DynamicModule> {
		const httpService = new HttpService();
		const proto = await firstValueFrom(httpService.get(options.protoUrl));
		const protoPackage = getPackage(proto.data);
		const protoDir: string = options.protoDir || "proto";
		!fs.existsSync(protoDir) && fs.mkdirSync(protoDir);
		const protoPath: string = `${protoDir}/${protoPackage}.proto`;
		fs.writeFileSync(protoPath, proto.data);
		const isGenerateInterfaces = options.generateInterfaces || false;
		isGenerateInterfaces && generateInterfaces(protoDir);
		return ClientsModule.register([{
			name: options.injectionTokenName,
			transport: Transport.GRPC,
			options: {
				protoPath,
				url: options.grpcUrl,
				package: protoPackage
			}
		}]);
	}
}
