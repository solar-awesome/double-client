import { spawnSync } from "child_process";

export function getPackage(proto: string): string {
	return proto.match("(?<=\\bpackage\\s)(\\w+)")[0];
}

export function generateInterfaces(protoPath: string): void {
	spawnSync("tsproto", [
		`--path=${protoPath}`
	]);
}