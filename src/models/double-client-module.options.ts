export interface DoubleClientModuleOptions {
	grpcUrl: string;
	protoUrl: string;
	protoDir?: string;
	injectionTokenName: string;
	generateInterfaces: boolean;
}